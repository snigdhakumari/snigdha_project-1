import csv
from incf.countryutils import transformations
import matplotlib.pyplot as plt

data = {'Asia':[],'Europe':[],'Africa':[],'North America':[],'Oceania':[],'Antartica':[],'South America':[]}


with open('Marriages.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        try:
            country = transformations.cn_to_ctn(row[0])
            index = data[country]
            index = index.append(row[1:6])
        except KeyError:
            pass

for list in data:
    try:
        year = []
        s_men = []
        s_women = []
        m_men = []
        m_women = []
        for yr in data[list]:
            year.append(int(yr[0]))
            m_women.append(float(yr[1]))
            m_men.append(float(yr[2]))
            s_women.append(float(yr[3]))
            s_men.append(float(yr[4]))
        plt.plot(m_women,year,'ro')
        plt.ylabel("years versus m-women")
        plt.axis([min(m_women),max(m_women),min(year),max(year)])
        plt.show()
        plt.plot(m_men,year,'ro')
        plt.ylabel("years versus m_men")
        plt.axis([min(m_men),max(m_men),min(year),max(year)])
        plt.show()
        plt.plot(s_women,year,'ro')
        plt.ylabel("years versus s_women")
        plt.axis([min(s_women),max(s_women),min(year),max(year)])
        plt.show()
        plt.plot(s_men,year,'ro')
        plt.ylabel("years versus s_men")
        plt.axis([min(s_men),max(s_men),min(year),max(year)])
        plt.show()
    except ValueError:
        pass
